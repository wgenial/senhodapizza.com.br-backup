<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="fragment" content="!" />

<?php 
if ( !isset ($_GET['_escaped_fragment_']) ){
						  echo '<meta name="description" content="A pizzaria Senhor da Pizza localizada na Vila Formosa, zona leste de São Paulo, oferece um cardápio completo com deliciosos sabores de pizzas, esfihas e porções" />';
					 }else {
						 switch ($_GET['_escaped_fragment_'] ) {
							case '/home' :
								echo '<meta name="description" content="A pizzaria Senhor da Pizza localizada na Vila Formosa, zona leste de São Paulo, oferece um cardápio completo com deliciosos sabores de pizzas, esfihas e porções" />';
							break; 
							case '/Localizacao' :
								echo '<meta name="description" content="Para um serviço de entrega mais eficiente, a pizzaria Senhor da Pizza atende por delivery em uma área que abrange até 10km quadrados. Dentro dessa área será cobrada uma pequena taxa de entrega. " />';
							break;
						 }
					 }
?>

<link href="favicon.ico" rel="icon" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="css/geral.css"/>
<link rel="stylesheet" type="text/css" href="css/fonts.css"/>
<link rel="stylesheet" type="text/css" href="css/slides.css"/>
<link rel="stylesheet" type="text/css" href="css/paginas.css"/>
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Gentium+Basic:400,700' rel='stylesheet' type='text/css' />

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/slides.min.jquery.js"></script>

<script type="text/javascript" src="js/css_browser_selector.js"></script>
<script type="text/javascript" src="js/jquery.address-1.4.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

		<script type="text/javascript">
            $(function(){
                $("#slides").slides({
					play: 5000
				});
				
				$("#delivery a").hover(function (e) {
					$(this).animate({color:'#ffca30'}, 500 )
				}, function (e) {
					$(this).animate({color:'#ffffff'}, 500 )
				});
				
				$("#menu ul li a").hover(function(e) {
						$(this).stop();
						$(this).animate({color:'#ffca30'}, 500 )
					}, function (e) {
						if (!$(this).hasClass('atual') ) {
							$(this).stop();
							$(this).animate({color:'white'}, 500 );
						}
					});
				$("#menu ul li a").click(function (e) {
					$("#menu ul li a").each(function(index, element) {
                        if ( $(this).hasClass('atual') ) {
							$(this).removeClass('atual');
							$(this).stop();
							$(this).animate({color:'white'}, 500 );
						}
                    });
					
					$(this).addClass('atual');
				});
					
				$("#desenvolvedor a").hover(function(e) {
					$(this).stop();
					$(this).animate({color:'#ffca30'}, 500 )
				}, function (e) {
					$(this).stop();
					$(this).animate({color:'red'}, 500 )
				});
				//$('a').address();
				//alert ($.address.data);
				
				$.address.change(function(event) {  

						if ( event.value == '/home' || event.value == '/' ) {
							Carrega('paginas/home.php');							
						}
						
						if ( event.value == '/A-Casa' ) {
							Carrega('paginas/a-casa.php');
						};
						
						if ( event.value == '/Produtos' ) {
							Carrega('paginas/produtos.php');
						};
						if ( event.value == '/Localizacao' ) {
							Carrega('paginas/localizacao.php');
						};
						if ( event.value == '/Sugestoes' ) {
							Carrega('paginas/sugestoes.php');
						};
						if ( event.value == '/Cadastro' ) {
							Carrega('paginas/cadastro.php');
						}
						if ( event.value == '/Fale-Conosco') {
							Carrega('paginas/fale-conosco.php');
						}
						if ( event.value == '/Trabalhe-Conosco') {
							Carrega('paginas/trabalhe-conosco.php');
						}
						if ( event.value == '/Area-de-Entrega' ) {
							Carrega('paginas/area-de-entrega.php');
						};
						if ( event.value == '/' ) {
							Carrega('paginas/home.php');
						};
						
						
						if ( event.value == "/Cardapio"){
							Carrega('paginas/cardapio.php');
							
							//Carrega_interna('paginas/cardapio/pizzas.php');
						};
						if ( event.value == "/Cardapio/Pizzas"){
							Carrega_interna('paginas/cardapio/pizzas.php')
						}
						if ( event.value == "/Cardapio/Pizzas-Doces"){
							Carrega_interna('paginas/cardapio/pizzas-doces.php')
						}
						
						if ( event.value == '/Cardapio/Pizzas-Especiais') {
							Carrega_interna('paginas/cardapio/pizzas-especiais.php')
						}
						if ( event.value == '/Cardapio/Esfihas') {
							Carrega_interna('paginas/cardapio/esfihas.php')
						}
						if ( event.value == '/Cardapio/Esfihas-Doces') {
							Carrega_interna('paginas/cardapio/esfihas-doce.php')
						}
						if ( event.value == '/Cardapio/Sobremesas') {
							Carrega_interna('paginas/cardapio/sobremesas.php')
						}
						if ( event.value == '/Cardapio/Bebidas') {
							Carrega_interna('paginas/cardapio/bebidas.php')
						}
						if ( event.value == '/Cardapio/Porcoes') {
							Carrega_interna('paginas/cardapio/porcoes.php')
						}
						
											
				});

            });
			
			
			
			function Carrega_interna ( pagina ) {
				$("#descricao").fadeOut(300, function (e) {
					
						
							$("#descricao").load(pagina,function (e) {
								//alert($('#descricao ul').height());
								$("#descricao").fadeIn(300, function (e) {});
								
								if ( pagina == 'paginas/cardapio/pizzas.php'  ) {
									if ( !$("#cardapio").children('li').first().children('a').hasClass('atual') ) {
										$("#cardapio").children('li').first().children('a').addClass('atual');
										$("#cardapio").children('li').first().children('a').mouseenter();
									}
								}
							});
						
					
				});
			}
			function Carrega ( pagina ) {
				//$("#pagina").css("height", $("#pagina").height() );
				$("#pagina").fadeOut(500, function (e) {
					$("#pagina").load(pagina,function (e) {
						
						$("#pagina").fadeIn(500, function (e) {
								if ( pagina == 'paginas/cardapio.php') {
									Carrega_interna('paginas/cardapio/pizzas.php');
								}
							});
						
					});
				});
			}
        </script>
<title><?php
					 if ( !isset ($_GET['_escaped_fragment_']) ){
						  echo 'Senhor da Pizza - Jd. Vila Formosa ';
					 }else {
						 switch ($_GET['_escaped_fragment_'] ) {
							case '/home' :
								echo 'Senhor da Pizza - Jd. Vila Formosa ';
							break;
							case '/A-Casa':
								echo 'Senhor da Pizza - A Casa ';
							break;							
							case '/Cardapio' :
								echo 'Senhor da Pizza - Cardápio ';
							break;
							case '/Produtos' :
								echo 'Senhor da Pizza - Conheça nossos Produtos ';
							break;
							
							case '/Localizacao' :
								echo 'Senhor da Pizza - Localização ';
							break;
							
							case '/Trabalhe-Conosco' :
								echo 'Senhor da Pizza - Trabalhe Conosco ';
							break;
							
							case '/Cadastro' :
								echo 'Senhor da Pizza - Jd. Vila Formosa ';
							break;

							case '/Fale-Conosco':
								echo 'Senhor da Pizza - Fale Conosco ';
							break;
							
							case '/Area-de-Entrega' :
								echo 'Senhor da Pizza - Area de Entrega ';
							break;
					
						
							case "/Cardapio":
								echo 'Senhor da Pizza - Nosso cardápio ';
							break;							
						 }
					 }
						 
						 
					 ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33757880-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></head>

<body>
	<header>
        <div id="delivery" style="height: 130px;">
        	<hgroup>
            	<h4 style="position:absolute; z-index:10000; text-align: right;">
            	<span style="display: block; font-size: 1.2em;">DELIVERY (11) 4118-1888</span>
                <span style="display: block; ">(11) 2154-9999</span>
	            <span> FAÇA SEU <a href="https://papdelivery.com.br/senhor-da-pizza?profile_id=1225" target="_blank">PEDIDO ONLINE</span></a>
              </h4>
            	<div class="l-social" style="">
            		<a class="l-social-link" href="http://www.facebook.com.br/senhordapizza" target="_blank">
            			<p class="l-social-text">curta nossa pagina no facebook</p>
            			<img class="l-social-img" src="http://www.senhordapizza.com.br/novo/imagens/icoFace.png" alt="facebook">
            		</a>
            	</div>
            </hgroup>
        </div>
        <div id="topo">
        	<section>
            	<h1 style="display:none;">SENHOR DA PIZZA PIZZARIA</h1>
	        	<a href="#/home" ><img src="imagens/topo/topo.png" alt="Logo Senhor Da Pizza"  /></a>
                <div id="slides">
                    <div class="slides_container">
                        <div>
                            <img src="http://www.senhordapizza.com.br/novo/imagens/banner/banner1.jpg" alt="Banner 1">
                        </div> 
                        <div>
                            <img src="http://www.senhordapizza.com.br/novo/banners/banner1.jpg" alt="Banner 2">
                        </div> 
                        <div>
                            <img src="http://www.senhordapizza.com.br/novo/banners/banner2.jpg" alt="Banner 3">
                        </div>
                    </div>
                    <div class="botao-flutuante" style="top: 228px;">
                      <a href="https://papdelivery.com.br/senhor-da-pizza?profile_id=1225" target="_blank" title="Faça seu pedido online">
                        <img src="imagens/botoes/botao-pedido-redondo.png" >
                      </a>
                    </div>
                </div>
                <div id="menu">
                	<ul>
                    	<li><a href="#!/A-Casa">A CASA</a></li>
                        <li><a href="#!/Cardapio">CARDÁPIO</a></li>
                        <li><a href="#!/Produtos">PRODUTOS</a></li>
                        <li><a href="#!/Localizacao">LOCALIZAÇÃO</a></li>
                        
                        <li><a href="#!/Area-de-Entrega">ÁREA DE ENTREGA</a></li>
                        <li><a href="#!/Cadastro">CADASTRO</a></li>
                        <li><a href="#!/Fale-Conosco">FALE CONOSCO</a></li>
                        
                        <li><a href="#!/Trabalhe-Conosco">TRABALHE CONOSCO</a></li>
                    </ul>
                </div>
        	</section>
        </div>
    </header>
    
    <div id="wrapper">
       	<div id="conteudo" style="padding-top: 320px;" >
        	<div id="interno">
            	<div id="esquerda">
                    <div id="promocao">
                        <h2>Promoção</h2>
                        <h3>CHOPP DOUBLE</h3>
                        <p>De segunda a quarta na compra de um chopp Brahma o segundo chopp é GRÁTIS! <span style="font-size:10px;">(válido para pedidos no salão)</span></p>
                        
                        <img src="imagens/background/paginas/home/bhrama.png" alt="Chopp Bhrama" style="margin-left:-25px; margin-top:-5px" />
                        
                        
                    </div>
                    <div id="vinhos">
                                <h3>VINHOS</h3>
                                <p>Venha conhecer nossa exclusiva carta de vinhos nacionais e importados.</p>
                    </div>
                </div>
                <div id="pagina">
	            	<?php
					 if ( !isset ($_GET['_escaped_fragment_']) ){
						  include 'paginas/home.php'; 
					 }else {
						 switch ($_GET['_escaped_fragment_'] ) {
							case '/home' :
								include 'paginas/home.php';
							break;
							case '/A-Casa':
								include 'paginas/a-casa.php';
							break;							
							case '/Cardapio' :
								include 'paginas/cardapio.php'; 
							break;
							case '/Produtos' :
								include 'paginas/produtos.php'; 
							break;
							
							case '/Localizacao' :
								include 'paginas/localizacao.php'; 
							break;
							
							case '/Trabalhe-Conosco' :
								include 'paginas/trabalhe-conosco.php'; 
							break;
							
							case '/Cadastro' :
								include 'paginas/cadastro.php';
							break;

							case '/Fale-Conosco':
								include 'paginas/fale-conosco.php';
							break;
							
							case '/Area-de-Entrega' :
								include 'paginas/area-de-entrega.php'; 
							break;
					
						
							case "/Cardapio":
								include 'paginas/cardapio.php';
							break;							
						 }
					 }
						 
						 
					 ?>
				</div>
            </div>
        </div>
        <div id="ilustrativo">
      		<!-- -->
        </div>
    </div>
  
   
   
    <footer>
	    <div id="center">
            <div id="aviso">
                <img class="aviso-proibido" src="imagens/background/footer/alchool-proibido-para-menores-de-18-anos.png" title="Alchool para menores é proibido" alt="Proibido a venda de Bebidas alchoólicas para menores de 18 anos" />
                <address id="endereco">
                    AV. JOÃO XXIII, 1130 <br>
                    VILA FORMOSA - SÃO PAULO <br>
                    FONE (11) 2154-9999 <br>

	                <span> FAÇA SEU <a style="color: rgb(255, 255, 255);" href="https://papdelivery.com.br/senhor-da-pizza?profile_id=1225" target="_blank">PEDIDO ONLINE</a></span>
                </address>
            </div>
            <p id="copyright">&copy; 2012. Senhor da Pizza. Todos os direitos reservados.</p>
            <p id="desenvolvedor">Desenvolvido por <a href="http://www.projectocom.com.br" target="_blank" title="Projecto Comunicação">Projecto Comunicação</a></p>
            <div id="footer_direita" >
                <div id="formas_pagamento" >
                    <p>Aceitamos</p>
                    <img src="imagens/background/footer/formas-de-pagamento.png" alt="MasterCard, Maestro, Visa, Visa Eletron, Visa Vale, Rede Shop, VR" />
                </div>
            </div>
        </div>
    </footer>
</body>
</html>