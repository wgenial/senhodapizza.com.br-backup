
<div id="nossa-casa" >
	 <div id="direita">
	<h3>TRABALHE-CONOSCO</h3>
    
        <p>Para enviar seu currículo basta preencher o formulário.</p>
        <form id="formulario" enctype="text/plain" action="paginas/formularios/fale-conosco-confirmado.php">
            <ul>
                <li><label>Nome: </label> <input name="nome" type="text" value="" class="required"  /></li>
                <li><label>E-mail: </label> <input name="e-mail" type="text" value="" class="required email"  /></li>
                <li><label>Telefone: </label> <input name="telefone" type="text" value="" class="required"  /></li>
                 <li><label>Cidade: </label> <input name="cidade" type="text" value="" class="required"  /></li>
                <li><label>Currículo:<span style="font-size:9px; color:#93191E;">(DOC ou PDF)</span> </label> <input name="curriculo" type="file" class="required" style="    margin-bottom: 10px; width: 200px;"  /></li>
                <li>
                    <label>Mensagem:</label><br />
                    <textarea name="mensagem" style="resize:none;" class="required" ></textarea>
                </li>
                <li><input type="button" value="enviar" class="enviar"  /></li>
                <li><p id="resposta" style="display:none;"></p></li>
            </ul>
        </form>
        
        <div id="progress"> </div>
    </div>
</div>
<script type="text/javascript">

$('#formulario').validate({
  rules: {
    curriculo: {
      required: true,
      accept: "doc|docx|pdf"
    }
  }
});
$('.enviar').click( function (e) {
	e.preventDefault();
	
	
	
		// inputted file path is not an image of one of the above types
//		alert("inputted file path is not an image!");
	
		if ($('#formulario').valid() == true){
			/*var $form  =  $( '#formulario' ),
			nome = $form.find( 'input[name="nome"]').val(),
			email = $form.find( 'input[name="e-mail"]').val(),
			telefone = $form.find( 'input[name="telefone"]').val(),
			cidade = $form.find('input[name="cidade"]' ).val(),
			mensagem = $form.find( 'textarea[name="mensagem"]').val();
			
			$.post('paginas/formularios/trabalhe-conosco-confirmado.php', { 
										'Nome':nome, 
										'Telefone':telefone,
										'E-mail':email,
										'Assunto':assunto,
										'Mensagem':mensagem									
										}, function (data) {$('#direita').html('<p>' + data + '</p>') } );*/
										
			var formData = new FormData($('form')[0]);
			$.ajax({
				url: 'paginas/formularios/trabalhe-conosco-confirmado.php',  //server script to process data
				type: 'POST',
				xhr: function() {  // custom xhr
					myXhr = $.ajaxSettings.xhr();
					if(myXhr.upload){ // check if upload property exists
						myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // for handling the progress of the upload
					}
					return myXhr;
				},
				//Ajax events
				beforeSend: beforeSendHandler,
				success: completeHandler,
				error: errorHandler,
				// Form data
				data: formData,
				//Options to tell JQuery not to process data or worry about content-type
				cache: false,
				contentType: false,
				processData: false
			});

		}
	
});

function beforeSendHandler (e ){
}
function completeHandler(e) {
	$('#direita').html('<p>' + e + '</p>');
}
function errorHandler(e) {
}
function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('#progress').attr({value:e.loaded,max:e.total});
    }
}


$('input').focusout(function(e) {
	var nome = $(this).attr('name'),
		valor = $(this).attr('value'),
		pagina = 'sugestoes';
		
    /*$.post('includes/sessions.php', { 
			nome : nome,
			valor : valor,
			pagina : pagina
		}, function (data) {
			$("#resposta").html( data )
		});*/

});
</script>