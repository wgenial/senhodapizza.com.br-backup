<div id="nossa-casa" >


<div id="direita">
	<h3>NOSSA CASA</h3>
    <p>A Senhor da Pizza traz para a zona leste de São Paulo os níveis e padrões de qualidade que a população da região merece. Com infraestrutura completamente adequada à modernidade, possuímos dois fornos à lenha para nossas deliciosas pizzas e um forno exclusivo para esfihas.<br />
<br />
Possuímos um amplo estacionamento com segurança para que você possa desfrutar tranquilamente de nossas deliciosas pizzas, esfihas, porções e para o happy hour nosso chopp Brahma e nossa exclusiva carta de vinhos . Como não poderia deixar de ser em um projeto 5 estrelas, são fundamentais os conceitos básicos em higiene. Toda nossa cozinha é revestida em granito e a utilização de luvas, máscaras e as tocas para os cabelos compõem o ambiente ideal para lidar com alimentação.<br />
<br />
A qualidade dos produtos que utilizamos é fundamental para a elaboração de nossas delícias. Por isso trabalhamos com o que há de melhor no mercado alimentício e contamos com profissionais experientes.<br />
<br />
O sistema delivery também é prioridade em nosso conceito. Contamos com uma equipe composta por quinze motoqueiros que agilizam nossas entregas. Dessa forma, você fica satisfeito por receber seu pedido em curto prazo, podendo assim deliciar-se com a encomenda que acabou de sair do forno.<br />
<br />
Aceitamos cartões de crédito, débito e todos os tickets. E para sua maior comodidade, disponibilizamos também o cartão VR Smart, Sodex e Bônus refeição (que poderão ser utilizados também para o pagamento em domicílio).<br />
<br />
Abrimos todos os dias, inclusive aos feriados, das 18h00 às 00h00. <br />
<br />
Bom Apetite!<br />
</p>

    </div>
    
    
</div>
