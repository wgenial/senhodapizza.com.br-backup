﻿<ul style="display:inline-block; margin-top:20px;">
<li><h4>Alemã</h4>
<p>Mussarela, calabresa, bacon e cebola</p></li>

	  	
<li><h4>Americana</h4>
<p>Mussarela, palmito, bacon, ovos e cebola</p></li>
 
<li><h4>Bacon</h4>
<p>Bacon, rodelas de tomate e cebola</p></li>

<li><h4>Bacon II</h4>
<p>Bacon, mussarela ou catupiry®</p></li>
 
<li><h4>Baiana</h4>
<p>Mussarela, palmito, bacon, ovos e cebola</p></li>
 
<li><h4>Calabresa</h4>
<p>Calabresa fatiada e cebola</p></li>

<li><h4>07 - Atum II R$</h4>
<p>Atum, mussarela ou Catupiry®</p></li>
 
<li><h4>10 - Bacalhau R$</h4>
<p>Bacalhau temperado, rodelas de tomate, mussarela ou Catupiry®</p></li>
 
<li><h4>11 - Bacon R$</h4>
<p>Bacon, rodelas de tomate e cebola</p></li>

<li><h4>12 - Bacon II R$</h4>
<p>Bacon, mussarela ou Catupiry®</p></li>
 
<li><h4>13 - Baiana R$</h4>
<p>Calabresa moída, pimenta, rodelas de tomate, parmesão ralado e cebola</p></li>
 
<li><h4>14 - Berinjela R$</h4>
<p>Berinjela temperada, cebola e mussarela</p></li>
 
<li><h4>15 -Senhor Pizza R$</h4>
<p>Lombo, champignon, cebola e mussarela</p></li>

 
<li><h4>16 -Brócolis R$</h4>
<p>Brócolis, alho, bacon, mussarela ou Catupiry®</p></li>
 
<li><h4>17 - Bufala R$</h4>
<p>Mussarela de búfala com tomate seco</p></li>
 
<li><h4>18 - Calabresa R$</h4>
<p>Calabresa e cebola</p></li>
 
<li><h4>19 - Calabresa II R$</h4>
<p>Calabresa fatiada com mussarela</p></li>
 
<li><h4>20 - Calabresa com bacon R$</h4>
<p>Calabresa, bacon e cebola</p></li>

<li><h4>21 - Camarão R$</h4>
<p>Camarão temperado, rodelas de tomate, mussarela ou Catupiry®</p></li>

<li><h4>22 - Catupiry® R$</h4>
<p>Catupiry®</p></li>
 
<li><h4>23 - Champignon R$</h4>
<p>Champignon, cebola e mussarela</p></li>

<li><h4>24 -Cinco queijos R$</h4>
<p>Mussarela, Catupiry®, provolone, parmesão ralado e gorgonzola</p></li>
 
<li><h4>25 - Da hora R$</h4>
<p>Presunto, champignon, bacon e Catupiry®</p></li>
 
<li><h4>26 - Do papa R$</h4>
<p>Catupiry®, rodelas de tomate, parmesão ralado e alho gratinado</p></li>
 
<li><h4>27 - Dois queijos R$</h4>
<p>Mussarela e Catupiry®</p></li>

<li><h4>28 - Escarola R$</h4>
<p>Verduras frescas temperadas, bacon ou aliche, cebola e mussarela</p></li>
 
<li><h4>29 - Forneiro R$</h4>
<p>Presunto, azeitonas verdes picadas e mussarela</p></li>
 
<li><h4>30 - Francesa R$</h4>
<p>Presunto, ovos, Catupiry® com mussarela</p></li>

<li><h4>31 - Frango caipira R$</h4>
<p>Peito de frango desfiado, milho e Catupiry®</p></li>
 
<li><h4>32 - Frango com Catupiry® R$</h4>
<p>Frango desfiado com Catupiry®</p></li>
 
 
<li><h4>34 - Jardineira R$</h4>
<p>Escarola, palmito, ervilhas, milho e mussarela</p></li>
 
 
<li><h4>35 - Light R$</h4>
<p>Mussarela de búfala, tomate seco, manjericão e parmesão ralado</p></li>

<li><h4>36 - Lombo R$</h4>
<p>Lombo defumado e cebola</p></li>
 
<li><h4>37 - Lombo II R$</h4>
<p>Lomgo defumado, mussarela ou Catupiry®</p></li>
 
<li><h4>38 - Marguerita R$</h4>
<p>Mussarela, molho de tomate, manjericão e parmesão ralado</p></li>
 
 
<li><h4>39 - Milho verde R$</h4>
<p>Milho verde e Catupiry®</p></li>
 
<li><h4>40 - Mussarela R$</h4>
<p>Mussarela</p></li>
 
<li><h4>41 - Napolitana R$</h4>
<p>Mussarela, rodelas de tomate e parmesão ralado</p></li>

<li><h4>42 - Palmito R$</h4>
<p>Palmito, cebola e mussarela</p></li>
 
<li><h4>43 - Paulista R$</h4>
<p>Mussarela, calabresa, rodelas de tomate e cebola</p></li>

<li><h4>44 - Peito de perú R$</h4>
<p>Peito de perú, mussarela ou Catupiry®</p></li>
 
<li><h4>45 - Peperoni R$</h4>
<p>Peperoni (salame especial condimentado com páprica) coberto com mussarela</p></li>

<li><h4>46 -Peruana R$</h4>
<p>Atum, palmito, ervilha, ovos, cebola e mussarela</p></li>
 
<li><h4>47 - Pizzaiolo R$</h4>
<p>Presunto, ervilha, champignon, ovos, Catupiry® e mussarela</p></li>

 
<li><h4>48 - Portuguesa R$</h4>
<p>Presunto, ovos, cebola e mussarela</p></li>
 
 
<li><h4>50 - Quatro queijos R$</h4>
<p>Mussarela, Catupiry®, provolone e parmesão ralado</p></li>
 
<li><h4>51 - Romana R$</h4>
<p>Mussarela, Filé de anchovas (importadas), rodelas de tomate e cebola</p></li>
 
 
<li><h4>52 - Rúcula R$</h4>
<p>Mussarela de búfala, tomate seco e rúcula</p></li>
 
 
<li><h4>54 - Siciliana R$</h4>
<p>Mussarela, champignon e bacon</p></li>
 
 
<li><h4>55 - Toscana R$</h4>
<p>Calabresa moída e mussarela</p></li>

 </ul>















<!-- 

CALABRESA (calabresa fatiada e cebola)....................................................................................................R$ 19,00 / R$ 25,90
CALABRESA COM BACON (calabresa fatiada, bacon e cebola).......................................R$ 23,00 / R$ 32,90
CALABRESA II (calabresa fatiada, mussarela ou catupiry®)...............................................................R$ 23,90 / R$ 33,90
CARNE SECA (carne seca, cebola, mussarela ou catupiry®).............................................................R$ 27,00 / R$ 38,90
CARPACCIO (mussarela, carpaccio, alcaparras, molho à base de mostarda e parmesão ralado)....R$ 27,00 / R$ 38,90
DA HORA (presunto, champignon, bacon e catupiry®)...........................................................................R$ 23,90 / R$ 33,90
FRANCESA (presunto, ovos, catupiry® e mussarela)..............................................................................R$ 25,00 / R$ 35,90
FORNEIRO (presunto, azeitonas verdes picadas, mussarela ou catupiry®)................................R$ 23,90 / R$ 33,90
HAVAIANA (mussarela, presunto e abacaxi em calda).........................................................................R$ 27,00 / R$ 38,90
LOMBO (lombo defumado e cebola)................................................................................................................R$ 22,00 / R$ 31,90
LOMBO II (lombo defumado, mussarela ou catupiry®)...........................................................................R$ 23,90 / R$ 33,90
MORTADELA ESPECIAL (mortadela, mussarela de búfala, tomate seco e rúcula).........R$ 27,00 / R$ 38,90
PARMA (presunto tipo parma e mussarela de búfala)................................................................................R$ 27,00 / R$ 38,90
PAULISTA (mussarela, calabresa, rodelas de tomate e cebola)...........................................................R$ 23,90 / R$ 33,90
PEPPERONI (pepperoni, mussarela ou catupiry®)...................................................................................R$ 27,00 / R$ 38,90
PIZZAIOLO (presunto, ervilha, champignon, ovos, mussarela e catupiry®).................................R$ 27,00 / R$ 38,90
PORTUGUESA (presunto, ovos, cebola, mussarela ou catupiry®)..................................................R$ 23,90 / R$ 33,90
SENHOR DA PIZZA (lombo, champignon, cebola e mussarela)...............................................R$ 25,00 / R$ 35,90
SICILIANA (mussarela, champignon e bacon)...........................................................................................R$ 23,90 / R$ 33,90
TOSCANA (calabresa moída, mussarela ou catupiry®)..........................................................................R$ 23,90 / R$ 33,90



ALHO E ÓLEO (mussarela, parmesão ralado e alho gratinado)...............................................R$ 23,90 / R$ 33,90
BÚFALA (mussarela de búfala com tomate seco)........................................................................................R$ 25,00 / R$ 35,90
CATUPIRY (catupiry®) ..............................................................................................................R$ 22,00 / R$ 31,90
CINCO QUEIJOS (mussarela, catupiry®, provolone, parmesão ralado e gorgonzola)........R$ 27,00 / R$ 38,90
DO PAPA (catupiry®, rodelas de tomate, parmesão ralado e alho gratinado)..................................R$ 23,90 / R$ 33,90
DOIS QUEIJOS (mussarela e catupiry®)...................................................................................................R$ 23,90 / R$ 33,90
LIGHT (mussarela de búfala, tomate seco, manjericão e parmesão ralado)...................................R$ 27,00 / R$ 38,90
MARGUERITA (mussarela, molho de tomate, manjericão e parmesão ralado).......................R$ 22,00 / R$ 31,90
MUSSARELA (mussarela) ............................................................................................................................R$ 19,00 / R$ 25,90
NAPOLITANA (mussarela, rodelas de tomate e parmesão ralado)...............................................R$ 22,00 / R$ 31,90
QUATRO QUEIJOS (mussarela, catupiry®, provolone e parmesão ralado)...........................R$ 26,00 / R$ 36,90
CAIPIRA (peito de frango desfiado, milho e catupiry®)..............................................................................R$ 25,00 / R$ 35,90
FRANGO COM CATUPIRY (peito de frango desfiado com catupiry®).............................R$ 23,90 / R$ 33,90
PEITO DE PERU (peito de peru, mussarela ou catupiry®).................................................................R$ 26,00 / R$ 36,90
ABOBRINHA (abobrinha, mussarela de búfala e parmesão)..............................................R$ 27,00 / R$ 38,90
ALCACHOFRA (fundo de alcachofra, alcaparras e mussarela)......................................................R$ 27,00 / R$ 38,90
À MODA DO CHEFE (escarola, presunto, rodelas de tomate e parmesão ralado).........R$ 23,90 / R$ 33,90
BERINJELA (berinjela temperada, cebola e mussarela).........................................................................R$ 23,00 / R$ 32,90
BRÓCOLIS (brócolis temperado com alho, bacon, mussarela ou catupiry®).................................R$ 23,90 / R$ 33,90
CHAMPIGNON (champignon, cebola e mussarela)........................................................................R$ 23,90 / R$ 33,90
COUVE (couve refogada, parmesão, mussarela ou catupiry®)...............................................................R$ 27,00 / R$ 38,90
ESCAROLA (escarola fresca temperada, bacon ou aliche, cebola e mussarela).........................R$ 23,00 / R$ 32,90
JARDINEIRA (escarola, palmito, ervilha, milho e mussarela)............................................................R$ 27,00 / R$ 38,90
MILHO VERDE (milho verde, catupiry® ou mussarela)......................................................................R$ 23,90 / R$ 33,90
PALMITO (palmito, cebola, mussarela ou catupiry®)................................................................................R$ 23,90 / R$ 33,90
REPOLHO ROXO (repolho roxo refogado, parmesão, mussarela ou catupiry®)..................R$ 27,00 / R$ 38,90
RÚCULA (mussarela de búfala, tomate seco e rúcula)............................................................................R$ 27,00 / R$ 38,90
SHITAKE (mussarela de búfala, shitake e tomate seco).........................................................R$ 27,00 / R$ 38,90
ALICHE (filés de anchova importada e parmesão ralado).......................................................................R$ 27,00 / R$ 38,90
ATUM (atum sólido e cebola) ............................................................................................................................R$ 22,00 / R$ 30,90
ATUM II (atum sólido, mussarela ou catupiry®)...........................................................................................R$ 24,00 / R$ 33,90
BACALHAU (bacalhau desfiado no azeite, rodelas de tomate, mussarela ou catupiry®)........R$ 38,00 / R$ 53,90
CAMARÃO (camarão temperado, rodelas de tomate, mussarela ou catupiry®)..........................R$ 38,00 / R$ 53,90
PERUANA (atum, palmito, ervilha, ovos, cebola, mussarela ou catupiry®).....................................R$ 27,00 / R$ 38,90
ROMANA (mussarela, filés de anchova importada, rodelas de tomate e cebola).......................R$ 27,00 / R$ 38,90
BANANA (banana, açúcar e canela).......................................................................................R$ 21,00 / R$ 30,90
BANANA ESPECIAL (banana, creme de sonho, açúcar, canela e chocolate)..................R$ 23,00 / R$ 32,90
CHOCOLATE (chocolate, leite condensado e granulado)...................................................R$ 23,00 / R$ 32,90
MORANGO (morango, chocolate e leite condensado) .......................................................................R$ 26,00 / R$ 36,90
PRESTÍGIO (chocolate, leite condensado e coco ralado)......................................................R$ 23,00 / R$ 32,90
ROMEU E JULIETA (goiabada, mussarela ou catupiry®)...................................................R$ 23,00 / R$ 32,90-->