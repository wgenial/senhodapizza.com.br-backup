
<div id="nossa-casa" >
	 <div id="direita">
	<h3>FALE-CONOSCO</h3>
    
        <p>Preencha o formulário selecionando o tipo de assunto que mais se aproxima<br />ao conteúdo da sua mensagem.</p>
        <form id="formulario" enctype="text/plain" action="paginas/formularios/fale-conosco-confirmado.php">
            <ul>
                <li><label>Nome: </label> <input name="nome" type="text" value="<? if (isset ( $_SESSION['sugestoes_e-mail'] ) ) { echo $_SESSION['sugestoes_e-mail']; } ?>" class="required"  /></li>
                <li><label>E-mail: </label> <input name="e-mail" type="text" value="<? if (isset ( $_SESSION['sugestoes_telefone'] ) ) { echo $_SESSION['sugestoes_telefone']; } ?>" class="required email"  /></li>
                <li><label>Telefone: </label> <input name="telefone" type="text" value="<? if (isset ( $_SESSION['sugestoes_nome'] ) ) { echo $_SESSION['sugestoes_nome']; } ?>" class="required"  /></li>
                <li>
                    <label>Assunto: </label><select name="assunto" >
                        <option value="Sugestões"> Sugestões </option>
                        <option value="Elogios"> Elogios </option>
                        <option value="Duvidas"> Dúvidas </option>
                        <option value="Críticas"> Críticas </option>
                        <option value="Críticas"> Outros </option>
                        
                    </select>
                </li>
                <li>
                    <label>Mensagem:</label><br />
                    <textarea name="mensagem" style="resize:none;" class="required" ></textarea>
                </li>
                <li>
                    <label>Cadastrar e-mail</label><br />
                    <input type="checkbox" id="enviarNews" name="enviarNews" value="sim" checked>
                </li>
                <li><input type="button" value="enviar" class="enviar"  /></li>
                <li><p id="resposta" style="display:none;"></p></li>
            </ul>
        </form>
    </div>
</div>
<script type="text/javascript">
$('#formulario').validate();
$('.enviar').click( function (e) {
	e.preventDefault();						
	if ($('#formulario').valid() == true){
		var $form  =  $( '#formulario' ),
		nome = $form.find( 'input[name="nome"]').val(),
		email = $form.find( 'input[name="e-mail"]').val(),
		telefone = $form.find( 'input[name="telefone"]').val(),
		assunto = $form.find('select[name="assunto"]' ).val(),
		mensagem = $form.find( 'textarea[name="mensagem"]').val();
		
		$.post('paginas/formularios/fale-conosco-confirmado.php', { 
									'Nome':nome, 
									'Telefone':telefone,
									'E-mail':email,
									'Assunto':assunto,
									'Mensagem':mensagem									
									}, function (data) {$('#direita').html('<p>' + data + '</p>') } );
	}
});


$('input').focusout(function(e) {
	var nome = $(this).attr('name'),
		valor = $(this).attr('value'),
		pagina = 'sugestoes';
		
    /*$.post('includes/sessions.php', { 
			nome : nome,
			valor : valor,
			pagina : pagina
		}, function (data) {
			$("#resposta").html( data )
		});*/

});
</script>