<div id="nossa-casa" >
	 <div id="direita">
	<h3>CADASTRO</h3>
    
        <p>Preencha corretamente o formulário abaixo para efetuar seu cadastro.<br /> Você participa das promoções em nosso site e ainda pode receber nossas novidades por e-mail.</p>
        <form id="formulario" enctype="text/plain" action="sugestoes_confirmada.php">
            <ul>
                <li><label>Nome : </label> <input name="nome" type="text" value=""  class="required" /></li>
                <li><label>E-mail : </label> <input name="e-mail" type="text" value="" class="required email" /></li>
                <li><label>Telefone : </label> <input name="telefone" type="text" value="" class="" /></li>
                <li><label>CEP :<a style="font-size:9px; color:#93191E" href="http://www.correios.com.br/servicos/cep/cep_loc_log.cfm" target="_blank">(Consultar) </a>  </label> <input name="CEP" type="text" value="" class="" /></li>
                <li><label>Endereço : </label> <input name="endereço" type="text" value="" class="" /></li>
                <li><label>Numero : </label> <input name="numero" type="text" value="" class="" /></li>
                <li><label>Complemento : </label> <input name="complemento" type="text" value="" /></li>
                <li><label>Bairro : </label> <input name="bairro" type="text" value="" class="" /></li>
                <li><label>Cidade : </label> <input name="cidade" type="text" value="" class="" /></li>
                <li><label>Estado : </label> <input name="estado" type="text" value="" class="" /></li>
				<li><input type="checkbox" checked="checked" value="1" name="newsletter"><label style="width:auto;" >Desejo receber as novidades do site por e-mail.</label></li>

                <li><input type="button" value="enviar" class="enviar" /></li>
            </ul>
        </form>
    </div>
</div>
<script type="text/javascript">
$('#formulario').validate();
$('.enviar').click( function (e) {
	e.preventDefault();						
	if ($('#formulario').valid() == true){
		var $form  =  $( '#formulario' ),
		nome = $form.find( 'input[name="nome"]').val(),
		email = $form.find( 'input[name="e-mail"]').val(),
		telefone = $form.find( 'input[name="telefone"]').val(),
		CEP = $form.find( 'input[name="CEP"]').val(),
		endereco = $form.find( 'input[name="endereço"]').val(),
		numero = $form.find( 'input[name="numero"]').val(),
		complemento = $form.find( 'input[name="complemento"]').val(),
		bairro = $form.find( 'input[name="bairro"]').val(),
		cidade = $form.find( 'input[name="cidade"]').val(),
		estado = $form.find( 'input[name="estado"]').val(),
		
		newsletter = $form.find('input[name="newsletter"]' ).attr('checked');
		if (newsletter) {
			newsletter = 'true';
		}else {
			newsletter = 'false';
		}
		
		$.post('paginas/formularios/cadastro-confirmado.php', { 
									'Nome':nome, 
									'E-mail':email,
									'Telefone':telefone,
									'CEP':CEP,
									'endereco':endereco,
									'numero':numero,
									'complemento':complemento,
									'bairro':bairro,
									'cidade':cidade,
									'estado':estado,
									'Newsletter':newsletter
									
									}, function (data) {$('#direita').html('' + data + '') } );
	}
});
</script>